package com.gestionservicios.miguel.gestor_de_servicios;


import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by root on 25/04/15.
 */
public class WebService {
    private static final String Metodo = "estadoActual";
    private static final String NameSpace = "https://web7.intrared.net:8083/ap/interf/app/Android/server.php";
    private static final String accionSoap = "urn:server_base#estadoActual";
    private static final String url = NameSpace+"?wsdl";
    SoapPrimitive resultado;
    public String obtenerDatos(int id){
       try {
           SoapObject request = new SoapObject(NameSpace,Metodo);
           request.addProperty("id",id);

           SoapSerializationEnvelope sobre = new SoapSerializationEnvelope(SoapEnvelope.VER11);
           sobre.setOutputSoapObject(request);

           HttpTransportSE transporte = new HttpTransportSE(url);

           transporte.call(accionSoap, sobre);

           resultado = (SoapPrimitive) sobre.getResponse();

           Log.i("Resultado", resultado.toString());
       }
       catch (Exception e){
           Log.e("Error", e.getLocalizedMessage());
       }
        return resultado.toString();
    }
}
